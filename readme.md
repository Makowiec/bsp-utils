# bsp-utils

This is a very basic conversion of Quake 2 map compilers so they can be used as DLLs in other applications. Based on original source from id with modifications by Geoffrey DeWan.
Keep in mind that this does **not** include arghrad and it produces very ugly lights.

### Compiling
The source should compile without any issues using Visual Studio 2017.

### Usage
Import DLLs into your project. To run a compiler exectute:

- RunQbsp(string args); for qbsp3.dll
- RunQrad(string args); for qrad3.dll
- RunQvis(string args); for qvis3.dll

The args string represents the standard command line argument set for each compiler. Compiler's output will be inserted into the console of your application.

### License
This is a derived work from [original source code](https://github.com/id-Software/Quake-2-Tools) by id Software and it shares its license.